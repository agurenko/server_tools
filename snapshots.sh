#!/bin/bash
if [[ ! $1 ]]
then
    echo "Usage:  ./snapshot.sh <action> <snapshot name>"
    echo "ERROR: Need a command"
    echo "Please use list, create, delete, update or revert"
    exit 1
fi

if [[ ! $1 == "list" ]]; then
    if [[ ! $2 ]]
        then
        echo "Usage:  ./snapshot.sh <action> <snapshot name>"
        echo "ERROR: Need a snapshot name"
        exit 1
    fi
else
    if [ "$2" == "--running" ]; then
        SCOPE="running"
    else
        SCOPE="all"
    fi
fi
# TODO: rework this as it's horrible
if [[ -n $3 ]]; then
    if [ "$3" == "--running" ]; then
        SCOPE="running"
    else
        SCOPE="all"
    fi
fi

function get_vms {

local PARAM="$1"

case "$PARAM" in
    all)
        virsh list --all | awk '{print $2}' | tail -n +3
        ;;
    running)
        virsh list --all | grep running | awk '{print $2}'
        ;;
    *)
        virsh list --all | awk '{print $2}' | tail -n +3
esac
}

function vm_get_state {

local VM=$1
local STATE=$(virsh list --all | grep $VM | awk '{print $3}')

echo "$STATE"
}

function vm_shutdown {

local VM=$1
local STATE=$(vm_get_state $VM)

if [ "$STATE" == "running" ]; then
    echo "$VM is running, need to shut it down first."
    virsh shutdown "$VM"
    printf "Waiting for %s to shutdown..." "$VM"
    while [[ $(virsh list | grep "$VM" | awk '{print $3}') == "running" ]]
    do
        sleep 1
    done
    echo "Done"
else
    echo "$VM is not running, no need to do anything."
fi
}

function vm_restore {

local VM=$1
local OLD_STATE=$2

case "$OLD_STATE" in
    running)
        virsh start "$VM"
        ;;
    *)
        echo "$VM was not running before"
esac
}

case "$1" in
    update)
        echo "Updating $2 snapshot"
        echo
        for vm in $(get_vms "$SCOPE")
        do
            echo "--------------------------------------------------------"
            echo "| Working with $vm"
            echo "--------------------------------------------------------"
            OLD_STATE=$(vm_get_state "$vm")
            vm_shutdown "$vm"
            virsh snapshot-delete "$vm" "$2"
            virsh snapshot-create-as "$vm" "$2"
            vm_restore "$vm" "$OLD_STATE"
            unset OLD_STATE
        done
        ;;
        
    revert)
        echo "Reverting to $2 snapshot"
        echo
        for vm in $(get_vms "$SCOPE")
        do
            echo "--------------------------------------------------------"
            echo "| Working with $vm"
            echo "--------------------------------------------------------"
            OLD_STATE=$(vm_get_state "$vm")
            virsh snapshot-revert "$vm" "$2"
            echo "$vm reverted"
            vm_restore "$vm" "$OLD_STATE"
            unset OLD_STATE
        done
        ;;
        
    create)
        echo "Creating snapshot $2"
        echo
        for vm in $(get_vms "$SCOPE")
        do
            echo "--------------------------------------------------------"
            echo "| Working with $vm"
            echo "--------------------------------------------------------"
            OLD_STATE=$(vm_get_state "$vm")
            vm_shutdown "$vm"
            virsh snapshot-create-as "$vm" "$2"
            vm_restore "$vm" "$OLD_STATE"
            unset OLD_STATE
        done
        ;;
    
    delete)
        echo "Deleting snapshot $2"
        echo
        for vm in $(get_vms "$SCOPE")
        do
            echo "--------------------------------------------------------"
            echo "| Working with $vm"
            echo "--------------------------------------------------------"
            virsh snapshot-delete "$vm" "$2"
        done
        ;;

    list)
        echo "Showing available snapshots"
        echo
        for vm in $(get_vms "$SCOPE")
        do
            echo "--------------------------------------------------------"
            echo "| Snapshots for $vm"
            echo "--------------------------------------------------------"
            virsh snapshot-list "$vm"
        done
        ;;

    *)
        echo "Unknown command"
        exit 1
esac