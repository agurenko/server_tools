#!/bin/bash
if [ -e $1 ]; then
    vm=$1
else
    echo "ERROR: VM name is required"
    echo "Please use ./$0 <vm_name> <new_vm_name>"
    exit 1
fi

if [ -e $2 ]; then
    new_vm=$2
else
    echo "ERROR: New VM name is required"
    echo "Please use ./$0 <vm_name> <new_vm_name>"
    exit 1
fi

virsh shutdown $vm

printf "Waiting for $vm to shutdown..."
while [[ `virsh list | grep $vm | awk '{print $3}'` == "running" ]]; do
    sleep 1
done
echo "Done"

# copy the storage.
cp /var/lib/libvirt/images/{$vm,$new_vm}.img

# dump the xml for the original
virsh dumpxml $vm > /tmp/$vm.xml

# hardware addresses need to be removed, libvirt will assign
# new addresses automatically
sed -i /uuid/d /tmp/$vm.xml
sed -i '/mac address/d' /tmp/$vm.xml

# and actually rename the vm: (this also updates the storage path)
sed -i s/$vm/$new_vm /tmp/$vm.xml

# finally, create the new vm
virsh define /tmp/$vm.xml
virsh start $vm
virsh start $new_vm
