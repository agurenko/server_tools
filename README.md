# server_tools
_This is simple collection of scripts that should help with day-to-day tasks_
# Script usage examples

**snapshot.sh:**

_Simple script to manage snapshots for all existing VMs_

    ./snapshot.sh create <snapshot name> [--running]
    ./snapshot.sh delete <snapshot name> [--running]
    ./snapshot.sh revert <snapshot name> [--running]
    ./snapshot.sh update <snapshot name> [--running]
    ./snapshot.sh list [--running]

**vm-clone.sh**

_Very basic script to clone existing VM on libvirt_

    ./vm-clone.sh <vm_name> <new_vm_name>